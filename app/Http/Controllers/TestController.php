<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function check(){


        $jsonString = file_get_contents(base_path('resources/lang//my.json'));

        $data = json_decode($jsonString, true);



        // Update Key

//        $data['country.title'] = "Change Manage Country";

        $data = [
            'productname'=>$_POST['pname'],
            'productquantity'=>$_POST['Quantity'],
            'price'=>$_POST['price'],


        ];
        $jsonString = file_get_contents(base_path('resources/lang//my.json'));

        $data = json_decode($jsonString, true);

        // Write File

        $newJsonString = json_encode($data, JSON_PRETTY_PRINT);

        file_put_contents(base_path('resources/lang/my.json'), stripslashes($newJsonString));


    //    dd(($data));
        foreach($data as $row)
        {
            $html =
                '<tr>'.
                  '<th>Product name</th>'.
                 '<td>' . $row->productname . '</td>' .
                '<th>Product Quantity</th>'.
                '<td>' . $row->productquantity . '</td>' .
                '<th>Product price</th>'.
                '<td>' . $row->price . '</td>' .
                '<th>Product Total amount</th>'.
                '<td>' . $row->price*$row->productquantity . '</td>' .
                '</tr>';
        }
        return $html;

    //    return view('form');


    }
}
