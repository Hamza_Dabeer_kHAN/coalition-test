<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2></h2>
    <form action="{{url('/submit_data')}}" method="post" enctype="multipart/form-data" id='comment'>
        {{csrf_field()}}
        <div class="form-group">
            <label for="Pname">Productname</label>
            <input type="text" class="form-control" id="PNAME" placeholder="Enter ProducT Name" name="pname">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity in Stock</label>
            <input type="text" class="form-control" id="pwd" placeholder="Enter Quantity " name="Quantity">
        </div>
        <div class="checkbox">
            <label><input type="text" name="price"> Price Per Item</label>
        </div>


        <input type='submit' name='submit' value="submit" maxlength="50" /><br>
        {{--<button id="btn">submit</button>--}}
    </form>
</div>
<script>
    jQuery( document ).ready( function( $ ) {

        $( '#comment' ).on( 'submit', function(e) {
            e.preventDefault();

            var name = $(this).find('input[name=name]').val();
            var Quantity = $(this).find('input[Quantity=Quantity]').val();
            var price = $(this).find('input[price=price]').val();

            $.ajax({
                type: "POST",
                url: '/submit_data',
                data: { name:name, Quantity:Quantity, price:price },
                success:function(msg){
                    if(msg){
                        $("#html").html(msg);
                    }else{
                        $("#html").html('Not Available');
                    }
                }
            });
            });


    });

</script>
</body>
</html>